# MyCPUgov
 
Script to limit the CPU frequency when the power is unplugged/plugged.

## Usage

```bash
Usage: cpu_limit_freq [ -m | --max-unplugged ] [ -M | --max-plugged ]                                                                                                     
Variables:                                                                                                                                                                
  -m, --max-unplugged      max cpu frequency when power is unplugged                                                                                                       
  -M, --max-plugged        max cpu frequency when power is plugged
```
Example:
```bash
cpu_limit_freq.sh -m 800 -M 3800
```

## CRON

In order to use it as a CRON job, the script has to be executable:
```bash
chmod -x cpu_limit_freq.sh
```
Place the script in /usr/local/sbin.
As the script used the command ```cpupower``` it has to be run as root.
```bash
sudo crontab -e
```
Add to following line to execute the script every minutes:
```bash
* * * * * /usr/local/sbin/cpu_limit_freq.sh -m 800 -M 3800
```

## TODO
- [ ] Set max-plugged as optional 

