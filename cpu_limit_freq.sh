#!/bin/bash


MIN=unset
MAX=unset

usage()
{
  echo "Usage: cpu_limit_freq [ -m | --max-unplugged ] [ -M | --max-plugged ]"
  echo ""
  echo "Variables:"
  echo "  -m, --max-unplugged       max cpu frequency when power is unplugged"
  echo "  -M, --max-plugged         max cpu frequency when power is plugged"
  exit 2
}

# Parsing arguments
PARSED_ARGUMENTS=$(getopt -a -n cpu_limit_freq -o m:M: --long min:,max: -- "$@")

# Are the arguments valid?
VALID_ARGUMENTS=$?

# If the arguments are not valid then show the usage
if [ "$VALID_ARGUMENTS" != "0" ]; then
  usage
fi

# Set arguments
eval set -- "$PARSED_ARGUMENTS"
while :
do
  case "$1" in
    -m | --min) MIN="$2" ; shift 2 ;;
    -M | --max) MAX="$2"   ; shift 2 ;;
    # -- means the end of the arguments; drop this, and break out of the while loop
    --) shift; break ;;
    # If invalid options were passed, then getopt should have reported an error,
    # which we checked as VALID_ARGUMENTS when getopt was called...
    *) echo "Unexpected option: $1 - this should not happen."
       usage ;;
  esac
done

# Get the current state of the battery (dischargin, chargin, fully-charged)
STATE=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep state)

# Remove string
STATE=$(sed 's/state: //' <<<"$STATE")

# Set the cpu power for low and high frequencies
low="cpupower frequency-set -u $MIN Mhz"
high="cpupower frequency-set -u $MAX Mhz"

# If th state is not discharging then set to high frequency, low otherwise 
[ $STATE != "discharging" ] && eval "$high" || eval "$low"

